from typing import List, Dict
from shapely.geometry import LineString
gender_colors = [(0, 0, 0), (255, 0, 0), (0, 0, 255)]
gender_labels = ['unspecified', 'male', 'female']


class Object:
    def __init__(self, person_id=None, frame_idx=None, gender=None, bbox=None, score=None, footline=None):
        self.person_id = person_id
        self.frame_idx = frame_idx
        self.gender = gender
        self.bbox = bbox
        self.score = score
        self.footline = footline
        self.log = ''

    @classmethod
    def from_dict(cls, _dict):
        return cls(_dict['person_id'], _dict['frame_idx'], _dict['gender'], _dict['bbox'], _dict['score'])

    def to_dict(self):
        return {
            'person_id': int(self.person_id),
            'frame_idx': int(self.frame_idx),
            'gender': int(self.gender),
            'bbox': [int(v) for v in self.bbox],
            'score': float(self.score)
        }

    @property
    def gender_color(self):
        return gender_colors[self.gender] if self.gender is not None else (
            255, 255, 255)

    @property
    def gender_text(self):
        return gender_labels[self.gender] if self.gender is not None else gender_labels[0]

    def update(self, _object):
        self.frame_idx = _object.frame_idx
        self.gender = _object.gender
        self.bbox = bbox
        self.score = score

    @property
    def footpoint(self):
        return ((self.bbox[0] + self.bbox[2])//2, self.bbox[3]-1)

    @property
    def radius(self):
        return (self.bbox[2]-self.bbox[0])//4


class Person:
    def __init__(self, id=None, objs=None):
        self.id = id
        self.objs: List[Object] = objs if objs is not None else []
        for i, current_obj in enumerate(self.objs[1:]):
            previous_obj = self.objs[i-1]
            current_obj.footline = (
                current_obj.footpoint, previous_obj.footpoint)

    @classmethod
    def from_dict(cls, _dict):
        return cls(_dict['id'], [Object.from_dict(obj_dict) for obj_dict in _dict['objs']])

    def to_dict(self):
        return {
            'id': self.id,
            'objs': [obj.to_dict() for obj in self.objs]
        }

    def add(self, _obj: Object):
        if len(self.objs) > 0:
            _obj.footline = (_obj.footpoint, self.objs[-1].footpoint)
        self.objs.append(_obj)

    @property
    def gender(self):
        gender_dict = {0: 0, 1: 0, 2: 0}
        for obj in self.objs:
            if obj.gender in gender_dict:
                gender_dict[obj.gender] += 1
            else:
                gender_dict[obj.gender] = 1
        poll = list(gender_dict.items())
        poll.sort(key=lambda item: item[1], reverse=True)
        return poll[0][0]

    def get_obj_in_area(self, area_bbox):
        left, top, right, bottom = area_bbox

        def is_inside(obj):
            x, y = obj.footpoint
            return x > left and y > top and x < right and y < bottom
        return list(filter(is_inside, self.objs))

    def get_obj_in_frame_interval(self, start_frame, stop_frame):
        return list(filter(lambda obj: obj.frame_idx in range(start_frame, stop_frame), self.objs))


class Register:
    def __init__(self, persons=None,fps=None):
        self.persons: Dict[Person] = persons if persons is not None else {}
        self.fps = fps

    @classmethod
    def from_dict(cls, _dict):
        return cls({int(_id): Person.from_dict(person_dict) for _id, person_dict in _dict['persons'].items()})

    def to_dict(self):
        return {
            'persons': {_id: person.to_dict() for _id, person in self.persons.items()},
        }

    def regist(self, person: Person):
        if person.id in self.persons:
            self.persons[person.id].add(person)
        else:
            self.persons[person.id] = person

    def regists(self, persons: List[Person]):
        for person in persons:
            self.regist(person)

    def count_gender(self):
        gender_dict = {0: 0, 1: 0, 2: 0}
        for person in self.persons.values():
            if person.gender in gender_dict:
                gender_dict[person.gender] += 1
            else:
                gender_dict[person.gender] = 1
        return gender_dict

    @property
    def count_person(self):
        return len(self.persons)

    @property
    def total_obj(self):
        return sum([len(person.objs) for person in self.persons.values()])

    @property
    def mean_frame(self):
        if self.count_person == 0:
            return None
        else:
            return self.total_obj/self.count_person

    @property
    def max_frame_idx(self):
        return max([person.objs[-1].frame_idx for person in self.persons.values()])

    @property
    def longest_frame_spent(self):
        return max([len(person.objs) for person in self.persons.values()])

    @property
    def footpoints(self):
        footpoints = []
        for person in self.persons.values():
            for obj in person.objs:
                footpoints.append(obj.footpoint)
        return footpoints

    @property
    def footpoints_with_id(self):
        footpoints = []
        ids = []
        for person in self.persons.values():
            for obj in person.objs:
                footpoints.append(obj.footpoint)
                ids.append(obj.person_id)

        return footpoints, ids

    @property
    def radiuses(self):
        raduises = []
        for person in self.persons.values():
            for obj in person.objs:
                raduises.append(obj.radius)
        return raduises

    def get_in_area_register(self, area_bbox):
        in_area_persons = {_id: Person(person.id, person.get_obj_in_area(
            area_bbox)) for _id, person in self.persons.items()}
        in_area_persons = dict(filter(lambda elem: len(
            elem[1].objs) > 0, in_area_persons.items()))
        return Register(in_area_persons,self.fps)

    @property
    def objs(self):
        objs = []
        for person in self.persons.values():
            objs.extend(person.objs)
        return objs
    def get_objs_sort_by_frameidx(self, max_frame_idx=None):
        objs = {}
        for person in self.persons.values():
            for obj in person.objs:
                if obj.frame_idx in objs:
                    objs[obj.frame_idx] += [obj, ]
                else:
                    objs[obj.frame_idx] = [obj, ]
        if max_frame_idx is None:
            max_frame_idx = max(objs.keys())

        for frame_idx in range(max_frame_idx):
            if frame_idx not in objs:
                objs[frame_idx] = []
        return objs

    def get_frame_interval_register(self, start_frame, stop_frame):
        in_interval_persons = {_id: Person(person.id, person.get_obj_in_frame_interval(
            start_frame, stop_frame)) for _id, person in self.persons.items()}
        in_interval_persons = dict(filter(lambda elem: len(
            elem[1].objs) > 0, in_interval_persons.items()))
        return Register(in_interval_persons,self.fps)



class Entry:
    def __init__(self, ):
        self.income_outer_ids = set()
        self.outcome_outer_ids = set()

        self.income_inner_ids = set()
        self.outcome_inner_ids = set()

        self.income_objs = []
        self.outcome_objs = []

    def update_state(self, obj, inner_status, outer_status):
        _id = obj.person_id
        if outer_status is not None:
            if outer_status:
                self.income_outer_ids.add(_id)
            else:
                self.outcome_outer_ids.add(_id)
        if inner_status is not None:
            if inner_status:
                self.income_inner_ids.add(_id)
            else:
                self.outcome_inner_ids.add(_id)
        reject_inner_ids = self.income_inner_ids.intersection(
            self.outcome_inner_ids)
        self.income_inner_ids -= reject_inner_ids
        self.outcome_inner_ids -= reject_inner_ids

        reject_outer_ids = self.income_outer_ids.intersection(
            self.outcome_outer_ids)
        self.income_outer_ids -= reject_outer_ids
        self.outcome_outer_ids -= reject_outer_ids

        income_ids = self.income_inner_ids.intersection(
            self.income_outer_ids)
        self.income_inner_ids -= income_ids
        self.income_outer_ids -= income_ids
        if len(income_ids) > 0:
            self.income_objs.append(obj)

        outcome_ids = self.outcome_inner_ids.intersection(
            self.outcome_outer_ids)
        self.outcome_inner_ids -= outcome_ids
        self.outcome_outer_ids -= outcome_ids
        if len(outcome_ids) > 0:
            self.outcome_objs.append(obj)


class AreaEntry(Entry):
    def __init__(self, name, bbox):
        super().__init__()
        self.name = name
        self.bbox = bbox

        ratio = 0.05
        h, w = self.bbox[3] - self.bbox[1], self.bbox[2] - self.bbox[0]
        self.inner_bbox = [
            bbox[0]+(0.1*w), bbox[1]+(0.1*h), bbox[2]-(0.1*w), bbox[3]-(0.1*h)]
        self.inner_bbox = [int(v) for v in self.inner_bbox]

    @property
    def origin_coor(self):
        return tuple(self.bbox[:2])

    @property
    def end_coor(self):
        return tuple(self.bbox[2:4])

    def is_inside_bbox(self, bbox, point):
        left, top, right, bottom = bbox
        x, y = point
        return x >= left and y >= top and x <= right and y <= bottom

    def is_touch_bbox(self, bbox, point):
        left, top, right, bottom = bbox
        x, y = point
        return x in (left, right) or y in (top, bottom)

    def check_line_status(self, bbox, line):

        left, top, right, bottom = bbox

        line_string = LineString(
            [(left, top), (right, top), (right, bottom), (left, bottom), (left, top)])

        obj_string = LineString(list(line))
        is_intersect = line_string.intersects(obj_string)

        if is_intersect:
            p1, p2 = line
            is_p1_touch = self.is_touch_bbox(bbox, p1)
            is_p2_touch = self.is_touch_bbox(bbox, p2)
            if is_p2_touch:
                return None
            if is_p1_touch:
                return not self.is_inside_bbox(bbox, p2)
            else:
                return self.is_inside_bbox(bbox, p1)
        else:
            return None

    def try_move_across(self, obj):
        if obj.footline is None:
            return
        outer_status = self.check_line_status(self.bbox, obj.footline)
        inner_status = self.check_line_status(self.inner_bbox, obj.footline)
        self.update_state(obj, inner_status, outer_status)

    def analyse_move_across(self, register: Register):
        for obj in register.objs:
            self.try_move_across(obj)


class LineEntry(Entry):
    def __init__(self, name, inner_line_coor,outer_line_coor, is_inside_positive=True):
        super().__init__()
        self.name = name
       
        self.inner_line_coor =self.line_coor = inner_line_coor
        self.outer_line_coor = outer_line_coor
         

        self.across_objs_positive = []
        self.across_objs_negative = []
        self.is_inside_positive = is_inside_positive

    @property
    def origin_coor(self):
        return min(self.line_coor[0][0], self.line_coor[1][0]), min(self.line_coor[0][1], self.line_coor[1][1])

    @property
    def end_coor(self):
        return max(self.line_coor[0][0], self.line_coor[1][0]), max(self.line_coor[0][1], self.line_coor[1][1])

    def create_linear_function(p1, p2):
        x1, y1 = p1
        x2, y2 = p2
        if x2 == x1:
            def linear_function(x, y):
                return x-x1
            return linear_function
        m = (y2-y1)/(x2-x1)
        b = y1 - m*x1

        def linear_function(x, y):
            return m*x-y+b
        return linear_function

    def check_line_status(self, line, obj):
        if obj.footline is None:
            return
        line_string = LineString(list(line))
        obj_string = LineString(list(obj.footline))
        is_intersect = line_string.intersects(obj_string)
        if is_intersect:
            linear_function = LineEntry.create_linear_function(*self.line_coor)
            v = linear_function(*(obj.footline[0]))
            
            if v > 0:
                status = True
            elif v < 0:
                status = False
            else:status = None
            if status is not None and not self.is_inside_positive:status = not status
            return status
        else:
            return None
            

    def try_move_across(self, obj: Object):
        inner_status = self.check_line_status(self.inner_line_coor,obj)
        outer_status = self.check_line_status(self.outer_line_coor,obj)
        self.update_state(obj,inner_status,outer_status)

    def analyse_move_across(self, register: Register):
        for obj in register.objs:
            self.try_move_across(obj)

print('load model module')
