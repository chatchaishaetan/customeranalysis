from HumanDetection import heatmap as hm
from model import Object, Person, Register,Entry,LineEntry,AreaEntry

import cv2


def draw_obj(img, obj):
    cv2.rectangle(img, tuple(obj.bbox[:2]), tuple(
        obj.bbox[2:4]), color=obj.gender_color)
    fontface = cv2.FONT_HERSHEY_DUPLEX
    cv2.putText(img, f'{obj.gender_text} {str(obj.score)[:4]}', tuple(
        obj.bbox[:2]), fontface, 1, obj.gender_color, 2)
    cv2.putText(img, f'{obj.person_id}', tuple(
        obj.bbox[2:4]), fontface, 1, obj.gender_color, 2)
    if obj.footline is not None:
        cv2.line(img, obj.footline[0],
                 obj.footline[1], obj.gender_color, 2)
    cv2.putText(img, obj.log, ((
        obj.bbox[0]+obj.bbox[2])//2, (obj.bbox[1]+obj.bbox[3])//2), fontface, 0.25, (0, 0, 0), 1)

    return img


gender_labels = ['unspecified', 'male', 'female']
gender_colors = [(0, 0, 0), (255, 0, 0), (0, 0, 255)]


def draw_count_gender(img, gender_dict):

    height, width = img.shape[:2]
    fontface = cv2.FONT_HERSHEY_DUPLEX
    for i, (gender_class, n) in enumerate(gender_dict.items()):
        text = f'{gender_labels[gender_class]} {n}'
        cv2.putText(img, text, (width-len(text)*10, (i+1)*16),
                    fontface, 0.5, gender_colors[gender_class], 1)
    return img


def draw_average_time_spent(register, img,):
    height, width = img.shape[:2]
    cv2.putText(img, f'avg {str(register.mean_frame / register.fps)[:4] if register.mean_frame is not None else "None"} sec',
                (0, 32), cv2.FONT_HERSHEY_DUPLEX, 0.5, (0, 0, 0), 1)
    return img


def draw_count_gender_each_roi(register, img, areas):
    if areas is None:
        return
    for area in areas:
        left, top, right, bottom = area.bbox

        area_register = register.get_in_area_register(area.bbox)
        gender_dict = area_register.count_gender()

        cv2.rectangle(img, (left, top), (right, bottom), (0, 0, 128), 1)
        draw_count_gender(
            img[top:bottom, left:right], gender_dict)
        draw_average_time_spent(
            area_register, img[top:bottom, left:right])


def draw_count_entry(register, img, line_entries, areas):
    if line_entries is not None:
        for line_entry in line_entries:
            cv2.line(img, *line_entry.inner_line_coor, (0, 0, 128), 2)
            cv2.line(img, *line_entry.outer_line_coor, (0, 0, 128), 2)
            cv2.putText(
                img,
                f'in {len(line_entry.income_objs)} out {len(line_entry.outcome_objs)}',
                line_entry.end_coor,
                cv2.FONT_HERSHEY_DUPLEX,
                0.5,
                (0, 0, 0))
    if areas is not None:
        for area_entry in areas:
            cv2.rectangle(
                img, area_entry.origin_coor, area_entry.end_coor, (0, 0, 128), 1)
            cv2.rectangle(img, tuple(area_entry.inner_bbox[:2]), tuple(
                area_entry.inner_bbox[2:4]), (0, 0, 128), 1)
            cv2.putText(img,
                        f'in {len(area_entry.income_objs)} out {len(area_entry.outcome_objs)}',
                        area_entry.end_coor,
                        cv2.FONT_HERSHEY_DUPLEX,
                        0.5,
                        (0, 0, 0))


print('import visualize')
