# %%
# %load_ext autoreload
# %autoreload 2
from __future__ import absolute_import

import glob
import os
import time
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict, List
from copy import copy

import cv2
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import tqdm
import ujson
from openpyxl import Workbook
from PIL import Image
from scipy.misc import imresize

from skimage.transform import resize

from GenderRecognition import gender_model
from HumanDetection import PersonDetector as Detection
from HumanDetection import heatmap as hm
from model import Object, Person, Register, Entry, AreaEntry, LineEntry
from Tracking import person_tracker
import visualize
# %%
gender_colors = [(0, 0, 0), (255, 0, 0), (0, 0, 255)]
gender_labels = ['unspecified', 'male', 'female']

min_detection_confidence = 0.5
min_gender_confidence = 0.6

# %%
global_log = ''


class CustomerAnalyst:
    def __init__(self, input_dir, register_path=None, min_detection_confidence=min_detection_confidence, min_gender_confidence=min_gender_confidence):
        self.input_dir = input_dir
        self.tracker = person_tracker.PersonTracker()
        if register_path is not None:
            with open(register_path, 'r') as f:
                _dict = ujson.load(f)
                self.register = Register.from_dict(_dict)
        else:
            self.register = Register()

        self.bg_img = None

        vid_paths = sorted(glob.glob(self.input_dir+'/*.mp4'))
        tmp_vid = cv2.VideoCapture(vid_paths[0])
        _, self.bg_img = tmp_vid.read()
        self.fps = tmp_vid.get(cv2.CAP_PROP_FPS)
        self.register.fps = self.fps
        tmp_vid.release()
        self.height, self.width = self.bg_img.shape[:2]

        self.min_detection_confidence = min_detection_confidence
        self.min_gender_confidence = min_gender_confidence

        self.areas: List[AreaEntry] = None
        self.line_entries: List[LineEntry] = None

    def set_rois(self, rois=None):
        if rois is None:
            img = self.bg_img.copy()
            window_name = 'draw roi'
            cv2.namedWindow(window_name)
            cv2.moveWindow(window_name, 0, 0)
            rois = cv2.selectROIs(window_name, img)
            rois[:, 2:4] += rois[:, :2]

        self.areas = [AreaEntry(str(i+1), roi) for i, roi in enumerate(rois)]
        print(rois)

    def set_line_entries(self, line_entries: List[LineEntry]):
        self.line_entries = line_entries

    @property
    def entries(self):
        return self.areas+self.line_entries

    def analyse(self, output_dir, display=False, use_detection_checkpoint=True, batch_size=4):
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()

        self.register = Register(fps=self.fps)

        frame_height, frame_width = self.bg_img.shape[:2]
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        output_vid_path = output_dir / 'visualization.mp4'
        out = cv2.VideoWriter(str(output_vid_path), fourcc, self.fps,
                              (frame_width, frame_height))

        if display:
            window_name = 'visualization'
            cv2.namedWindow(window_name)
            cv2.moveWindow(window_name, 0, 0)
        seek = 0

        for vid_path in sorted(Path(self.input_dir).glob('*.mp4')):
            start_time = time.time()

            person_detector = Detection.PersonDetector(
                str(vid_path.parent / f'{vid_path.stem}.json'), use_detection_checkpoint, backbone='retina')

            vid = cv2.VideoCapture(str(vid_path))
            start_checkpoint_seek = 0
            objs = []
            is_end_video = False
            while not is_end_video:
                imgs = []
                for _ in range(batch_size):
                    ret, img = vid.read()
                    if ret:
                        imgs.append(img)
                    else:
                        print('end')
                        is_end_video = True
                        break
                if len(imgs) == 0:break
                batch_bboxes, batch_scores = person_detector.detect_persons(
                    imgs, start_checkpoint_seek, min_detection_confidence)

                for img,bboxes, scores in zip(imgs,batch_bboxes, batch_scores):
                    result = self.tracker.update(img, bboxes, scores)
                    for i, (_id, left, top, right, bottom) in enumerate(result):

                        if left >= right or top >= bottom:
                            continue

                        bbox = left, top, right, bottom = [max(0, left), max(0, top), min(
                            self.width, right), min(self.height, bottom)]

                        obj_img = img[top:bottom, left:right]
                        if 0 in obj_img.shape:
                            continue
                        print(_id, bbox, obj_img.shape[:2])

                        gender, confidence = gender_model.predict_img_with_confidence(
                            obj_img)
                        if confidence < self.min_gender_confidence:
                            gender = 0
                            confidence = -1

                        obj = Object(person_id=_id, frame_idx=seek, bbox=bbox,
                                     gender=gender, score=confidence)
                        objs.append(obj)

                        visualize.draw_obj(img, obj)

                        if _id in self.register.persons:
                            self.register.persons[_id].add(obj)
                        else:
                            self.register.persons[_id] = Person(
                                _id, objs=[obj])

                    # visualize.draw_count_gender(img, self.register.count_gender())
                    self.draw_overlay(img, self.register)
                    out.write(img)
                    seek += 1
                    start_checkpoint_seek += 1

                    if display:
                        cv2.imshow(window_name, img)
                        if cv2.waitKey(1) == ord('q'):
                            cv2.destroyAllWindows()
                            break
            if use_detection_checkpoint:
                person_detector.save_detection_json()

            print('[elapse]', vid_path, time.time()-start_time, 'secs')
        out.release()
        with open(str(output_dir/'register.json'), 'w') as f:
            ujson.dump(self.register.to_dict(), f, indent=2)
        self.export_result(output_dir)

    def heatmap(self, _range=None, n=6, grid_size=8):

        bg_img = self.bg_img.copy()
        height, width = bg_img.shape[:2]
        heatmap_data = hm.process_heatmap(
            self.register.footpoints, self.register.radiuses, width, height, grid_size=grid_size)

        if _range is not None:
            thresholds = hm.calculate_thresholds(
                (_range[0], _range[1]), n)
        else:
            thresholds = hm.calculate_thresholds(
                (0, np.max(heatmap_data)//self.fps), n)

        bg_img = hm.draw_heatmap_to_img(
            heatmap_data, bg_img, thresholds, _range=_range, unit='sec')
        return bg_img, heatmap_data

    def heatmap_by_n(self, _range=None, n=6, grid_size=32):
        bg_img = self.bg_img.copy()
        height, width = bg_img.shape[:2]
        footpoints, ids = self.register.footpoints_with_id
        heatmap_data = hm.process_heatmap_by_n(
            footpoints, ids, width, height, grid_size=grid_size)

        if _range is not None:
            thresholds = hm.calculate_thresholds(
                (_range[0], _range[1]), n)
        else:
            thresholds = hm.calculate_thresholds(
                (0, np.max(heatmap_data)), n)
        bg_img = hm.draw_heatmap_to_img(
            heatmap_data, bg_img, thresholds, _range=_range, unit='people')
        return bg_img, heatmap_data

    def export_result(self, output_dir):
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()

        img = self.bg_img.copy()
        visualize.draw_count_gender(img, self.register.count_gender())

        if self.areas is not None:
            visualize.draw_count_gender_each_roi(
                self.register, img, self.areas)

        cv2.imwrite(str(output_dir/'count_people.jpg'), img)

        heatmap_img, heatmap_data = self.heatmap()
        cv2.imwrite(str(output_dir/'heatmap.jpg'), heatmap_img)
        heatmap_img, heatmap_data = self.heatmap_by_n()
        cv2.imwrite(str(output_dir/'heatmap_by_n.jpg'), heatmap_img)

        with open(str(output_dir/'count_gender.txt'), 'w') as f:
            gender_dict = self.register.count_gender()
            for gender_class, n in gender_dict.items():
                f.write(
                    f'{gender_class}.{gender_labels[gender_class]}\t{n} person{"s" if n > 1 else ""}\n')

    def draw_overlay(self, img, register):
        visualize.draw_count_gender(img, register.count_gender())
        visualize.draw_count_gender_each_roi(register, img, self.areas)
        visualize.draw_average_time_spent(register, img)
        visualize.draw_count_entry(
            register, img, self.line_entries, self.areas)
        cv2.putText(
            img, global_log, (0, img.shape[0]//2), cv2.FONT_HERSHEY_DUPLEX, 0.3, (0, 0, 0), 1)

    def visualize(self, output_path=None, display=True, start_time=0, stop_time=None):

        seek = 0
        start_seek = int(start_time * self.fps)
        if stop_time is not None:
            stop_seek = int(stop_time*self.fps)
        else:
            stop_seek = None
        objs_pool = self.register.get_objs_sort_by_frameidx()
        register = Register(fps=self.fps)

        if display:
            window_name = 'visualize'
            cv2.namedWindow(window_name)
            cv2.moveWindow(window_name, 0, 0)

        if output_path is not None:
            output_path = Path(output_path)
            if not output_path.parent.exists():
                output_path.parent.mkdir()
            frame_height, frame_width = self.bg_img.shape[:2]
            fourcc = cv2.VideoWriter_fourcc(*'XVID')

            out = cv2.VideoWriter(str(output_path), fourcc, self.fps,
                                  (frame_width, frame_height))
        for vid_path in sorted(Path(self.input_dir).glob('*.mp4')):
            vid = cv2.VideoCapture(str(vid_path))
            frame_count = vid.get(cv2.CAP_PROP_FRAME_COUNT)
            if start_seek != -1:
                if start_seek > frame_count:
                    start_seek -= frame_count
                    if stop_seek is not None:
                        stop_seek -= frame_count
                    seek += frame_count
                    continue
                else:
                    vid.set(cv2.CAP_PROP_POS_FRAMES, start_seek)
                    seek += start_seek
                    start_seek = -1
            is_break = False
            while True:
                ret, img = vid.read()
                if not ret:
                    break
                if seek not in objs_pool:
                    break
                objs = objs_pool[seek]
                for obj in objs:
                    _id = obj.person_id
                    if _id in register.persons:
                        register.persons[_id].add(obj)
                    else:
                        register.persons[_id] = Person(_id, objs=[obj])
                    for entry in self.entries:
                        entry.try_move_across(obj)
                    visualize.draw_obj(img, obj)
                self.draw_overlay(img, register)
                if display:
                    cv2.imshow(window_name, img)
                    if cv2.waitKey(1) == ord('q'):
                        cv2.destroyAllWindows()
                        return
                if output_path is not None:
                    out.write(img)

                seek += 1
                if seek > stop_seek:
                    is_break = True
                    break
                if seek % 100 == 0:
                    print(seek//self.fps)
            if is_break:
                break
        if output_path is not None:
            out.release()

    def export_interval_result(self, output_path, start_datetime, interval_time=15*60,):
        wb = Workbook()
        area_sheet = wb.create_sheet('Area', 0)
        header = ['Zone ID', 'Total', 'Total male', 'Total female', 'Total unspecified',
                  'Average time spent per person (seconds)', 'Max time spent (seconds)', 'Start time', 'End time', 'Interval time (seconds)']
        area_sheet.append(header)

        entry_sheet = wb.create_sheet('Entry', 1)
        header = ['Entry name', 'Total income', 'Total male income',
                  'Total female income', 'Total unspecified income',
                  'Total outcome', 'Total male outcome', 'Total female outcome',
                  'Total unspecified outcome', 'Start time', 'End time',
                  'Interval time (seconds)']
        entry_sheet.append(header)

        def create_area_row(register, zone_id, start_frame_idx, end_frame_idx):
            gender_dict = register.count_gender()
            row = [zone_id,
                   register.count_person,
                   gender_dict[1],
                   gender_dict[2],
                   gender_dict[0],
                   register.mean_frame/self.fps,
                   register.longest_frame_spent/self.fps,
                   start_datetime +
                   timedelta(seconds=start_frame_idx/self.fps),
                   start_datetime + timedelta(seconds=end_frame_idx/self.fps),
                   (end_frame_idx-start_frame_idx)/self.fps]
            return row

        def create_entry_row(entry, start_frame_idx, end_frame_idx):

            def get_gender_dict(objs):
                gender_dict = {i: 0 for i in range(len(gender_labels))}
                for obj in objs:
                    gender_dict[self.register.persons[obj.person_id].gender] += 1
                return gender_dict
            income_gender_dict = get_gender_dict(entry.income_objs)
            outcome_gender_dict = get_gender_dict(entry.outcome_objs)
            row = [
                entry.name,
                len(entry.income_objs),
                income_gender_dict[1],
                income_gender_dict[2],
                income_gender_dict[0],
                len(entry.outcome_objs),
                outcome_gender_dict[1],
                outcome_gender_dict[2],
                outcome_gender_dict[0],
                start_datetime +
                timedelta(seconds=start_frame_idx/self.fps),
                start_datetime + timedelta(seconds=end_frame_idx/self.fps),
                (end_frame_idx-start_frame_idx)/self.fps

            ]
            return row

        interval_n_frame = int(interval_time*self.fps)
        for start_frame_idx in range(0, self.register.max_frame_idx, interval_n_frame):
            end_frame_idx = min(
                start_frame_idx+interval_n_frame, self.register.max_frame_idx)
            interval_register = self.register.get_frame_interval_register(
                start_frame_idx, end_frame_idx)
            row = create_area_row(interval_register, '0',
                                  start_frame_idx, end_frame_idx)
            area_sheet.append(row)

            for area in self.areas:
                area_interval_register = interval_register.get_in_area_register(
                    area.bbox)
                row = create_area_row(area_interval_register,
                                      area.name, start_frame_idx, end_frame_idx)
                area_sheet.append(row)

            for entry in self.entries:
                entry = copy(entry)
                entry.analyse_move_across(interval_register)
                row = create_entry_row(
                    entry, start_frame_idx, end_frame_idx)
                entry_sheet.append(row)

        row = create_area_row(self.register, '0', 0,
                              self.register.max_frame_idx)
        area_sheet.append(row)

        for area in (self.areas):
            area_register = self.register.get_in_area_register(
                area.bbox)
            row = create_area_row(area_register, area.name, 0,
                                  self.register.max_frame_idx)
            area_sheet.append(row)

        for start_frame_idx in range(0, self.register.max_frame_idx, interval_n_frame):
            end_frame_idx = min(
                start_frame_idx+interval_n_frame, self.register.max_frame_idx)
            interval_register = self.register.get_frame_interval_register(
                start_frame_idx, end_frame_idx)

        for entry in self.entries:
            entry = copy(entry)
            entry.analyse_move_across(self.register)
            row = create_entry_row(entry, 0, self.register.max_frame_idx)
            entry_sheet.append(row)
        wb.save(str(output_path))


# %%
analyst = CustomerAnalyst(
    'dataset/CCTV', register_path='dataset/CCTV/output/register.json')
# analyst.analyse('dataset/CCTV/output')

analyst.set_rois(np.array([[296, 227, 546, 358],
                           #   [3, 428, 214, 569],
                           #   [418, 406, 673, 573]
                           ]))
# analyst.set_line_entries(
#     [LineEntry('Entrance', ((285, 230), (472, 214)), is_inside_positive=False)])
analyst.set_line_entries(
    [LineEntry('Entrance', ((281, 244), (481, 223)), ((285, 210), (472, 194)), is_inside_positive=False)])

# analyst.visualize(output_path='test5.mp4',
#                   start_time=42, stop_time=50)
analyst.visualize(output_path='tmp/testv2_0.mp4',
                  start_time=5*60, stop_time=5*60+45)
# %%

analyst = CustomerAnalyst(
    'dataset/test_CCTV', )
analyst.analyse('dataset/test_CCTV/test_output_conf-0',
                display=True, use_detection_checkpoint=True)

# %%
