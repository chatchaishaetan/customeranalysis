# %%
from HumanDetection.yolo import YOLO

from PIL import Image, ImageDraw, ImageFont
import matplotlib.pyplot as plt
import glob
import os
import random
import numpy as np
import cv2
import tqdm
import ujson
from pathlib import Path
# %%
module_path = os.path.dirname(os.path.abspath(__file__))
root_model_path = module_path + '/' + 'model_data/coco'
model = YOLO(model_path=root_model_path + '/weight.h5', anchors_path=root_model_path +
             '/anchor.txt', classes_path=root_model_path + '/classes.txt')
gender_annotation_path = f'{root_model_path}/gender_annotation.txt'
# %%


def create_predict_files(input_dir, predict_dir):
    with open(gender_annotation_path, 'r') as f:
        lines = f.read().split('\n')
        img_paths = [line.split(' ')[0] for line in lines]

    with open(root_model_path+'/classes.txt', 'r') as f:
        categories = f.read().split('\n')

    if not os.path.exists(predict_dir):
        os.makedirs(predict_dir)
    # img_paths = glob.glob(f'{input_dir}/*.jpg')
    for i, img_path in enumerate(img_paths):
        img = Image.open(img_path)
        output_img, bboxes, scores, classes = model.detect_image(img)
        basename = os.path.basename(img_path)
        filename = basename.split('.')[0]
        predict_path = f'{predict_dir}/{filename}.txt'
        print(f'create text {i+1}/{len(img_paths)}')
        with open(predict_path, 'w') as f:
            f.write('\n'.join(
                [f'{categories[classes[i]]} {scores[i]} { " ".join([str(int(v)) for v in bboxes[i]]) }' for i in range(len(bboxes))]))
# %%


def create_predict_imgs(input_dir, predict_dir, n=None):
    if not os.path.exists(predict_dir):
        os.makedirs(predict_dir)
    img_paths = glob.glob(f'{input_dir}/**/*.jpg')
    np.random.shuffle(img_paths)
    n = len(img_paths) if n is None else n
    img_paths = img_paths[:n]
    for img_path in img_paths:
        img = Image.open(img_path)
        output_img, bboxes, scores, classes = model.detect_image(img)
        output_img.save(f'{predict_dir}/{os.path.basename(img_path)}')
# %%


def random_predict(input_dir):
    img_paths = glob.glob(f'{input_dir}/**/*.jpg')
    img_path = img_paths[random.randint(0, len(img_paths))]
    img = Image.open(img_path)
    plt.imshow(img)
    output_img, bboxes, scores, classes = model.detect_image(img, verbose=True)
    plt.imshow(output_img)

# %%


def draw_label(pil_img, bboxes, scores, classes, categories, colors):

    font = ImageFont.truetype(font='font/FiraMono-Medium.otf',
                              size=np.floor(3e-2 * pil_img.size[1] + 0.5).astype('int32'))
    thickness = (pil_img.size[0] + pil_img.size[1]) // 300

    for i in range(len(bboxes)):
        c = classes[i]
        predicted_class = categories[c]
        box = bboxes[i]
        score = scores[i]

        label = '{} {:.2f}'.format(predicted_class, score)
        draw = ImageDraw.Draw(pil_img)
        label_size = draw.textsize(label, font)

        # top, left, bottom, right = box
        left, top, right, bottom = box
        top = max(0, np.floor(top + 0.5).astype('int32'))
        left = max(0, np.floor(left + 0.5).astype('int32'))
        bottom = min(pil_img.size[1], np.floor(bottom + 0.5).astype('int32'))
        right = min(pil_img.size[0], np.floor(right + 0.5).astype('int32'))

        if top - label_size[1] >= 0:
            text_origin = np.array([left, top - label_size[1]])
        else:
            text_origin = np.array([left, top + 1])

        # My kingdom for a good redistributable image drawing library.
        for i in range(thickness):
            draw.rectangle(
                [left + i, top + i, right - i, bottom - i],
                outline=colors[c])
        draw.rectangle(
            [tuple(text_origin), tuple(text_origin + label_size)],
            fill=colors[c])
        draw.text(text_origin, label, fill=(0, 0, 0), font=font)
    return pil_img


def ranged_bbox(bbox, img_shape):
    left, top, right, bottom = bbox
    left = max(0, left)
    top = max(0, top)
    right = min(img_shape[1], right)
    bottom = min(img_shape[0], bottom)
    return left, top, right, bottom


def crop_img(img, bbox):
    left, top, right, bottom = bbox
    left = max(0, left)
    top = max(0, top)
    right = min(img.shape[1], right)
    bottom = min(img.shape[0], bottom)
    return img[top:bottom, left:right]
# %%


def create_predict_video(input_path, output_path, visualize=False, logging=False):

    from GenderRecognition import gender_model
    categories = gender_model.categories
    colors = ((0, 0, 0, 128), (0, 0, 255, 128), (255, 0, 0, 128))

    vid = cv2.VideoCapture(input_path)
    fps = 20
    frame_width = int(vid.get(3))
    frame_height = int(vid.get(4))
    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    out = cv2.VideoWriter(output_path, fourcc, fps,
                          (frame_width, frame_height))
    seek = 0
    if visualize:
        window_name = 'f'
        cv2.namedWindow(window_name)
        cv2.moveWindow(window_name, 0, 0)
    dets_path = f'{os.path.dirname(output_path)}/{os.path.basename(output_path).split(".")[0]}.json'
    dets = []
    while True:
        ret, frame = vid.read()
        if not ret:
            break
        pil_img = Image.fromarray(frame[:, :, ::-1])
        out_boxes, out_scores, out_classes = model.predict(
            pil_img)
        out_boxes = np.array(
            [np.array([bbox[1], bbox[0], bbox[3], bbox[2]]) for bbox in out_boxes])
        person_bboxes = out_boxes[out_classes == 0].astype('int')
        person_scores = out_scores[out_classes == 0]
        if len(person_bboxes) > 0:
            person_imgs = [crop_img(frame, bbox) for bbox in person_bboxes]
            person_classes = gender_model.predict_imgs(person_imgs)

            pil_output_img = draw_label(
                pil_img, person_bboxes, person_scores, person_classes, categories, colors)
            output_img = np.array(pil_output_img)[:, :, ::-1]
            det = np.concatenate(
                (person_bboxes, np.expand_dims(person_scores, axis=0).T), axis=1)
            det = det.tolist()
        else:
            output_img = frame
            det = []
        dets.append(det)
        out.write(output_img)
        if visualize:
            cv2.imshow(window_name, cv2.resize(
                output_img, None, fx=0.5, fy=0.5))
            if cv2.waitKey(1) == ord('q'):
                cv2.destroyAllWindows()
                break
        seek += 1
        print(seek)
    vid.release()
    out.release()
    ujson.dump(dets, open(dets_path, 'w'))
# %%


def create_predict_videos(input_dir, output_dir, logging=True):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    for input_path in sorted(glob.glob(input_dir+'/*.mp4')):
        output_path = f'{output_dir}/{os.path.basename(input_path).split(".")[0]}.mp4'
        create_predict_video(input_path, output_path, logging=logging)
# %%
