# %%
import os
import cv2
import ujson
from scipy.io import loadmat
import numpy as np
import pickle
import glob
# %%
root_model_path = 'model_data/coco'
gender_annotation_path = f'{root_model_path}/gender_all_annotation.txt'

# %%
# def convert_yolo_dataset( output_img_dir, output_annotation_path):
output_img_dir = 'dataset_bbox/Image'
output_annotation_path = 'dataset_bbox/Annotation/yolo_annotation.json'
with open(gender_annotation_path, 'r') as f:
    lines = f.read().split('\n')
    img_paths = [line.split(' ')[0] for line in lines]

with open(root_model_path+'/classes.txt', 'r') as f:
    categories = f.read().split('\n')

if not os.path.exists(output_img_dir):
    os.makedirs(output_img_dir)
if not os.path.exists(os.path.dirname(output_annotation_path)):
    os.makedirs(os.path.dirname(output_annotation_path))
annotation = []  # {'filename':str, 'class":int, 'confidence':float}
i = 0
for img_idx, line in enumerate(lines):
    img_path, *bboxes = line.split(' ')
    # img = cv2.imread(img_path)
    for bbox_idx, bbox_text in enumerate(bboxes):
        bbox_img_filename = f'yolo_{str(i).rjust(7,"0")}.jpg'
        bbox = [int(v) for v in bbox_text.split(',')]
        x1, y1, x2, y2, class_idx = bbox
        # cropped_img = img[y1:y2, x1:x2]
        # cv2.imwrite(f'{output_img_dir}/{bbox_img_filename}', cropped_img)
        annotation.append({
            'filename': bbox_img_filename,
            'class': class_idx
        })
        i += 1
with open(output_annotation_path, 'w') as f:
    ujson.dump(annotation, f)

#%%

# %%
annotation_path = 'dataset/PA-100K/annotation/annotation.mat'
output_annotation_path = 'dataset_bbox/Annotation/pa100k_annotation.json'
# def convert_pa100k_dataset(annotation_path,output_annotation_path):

anno = loadmat(annotation_path)
image_annos = anno['train_images_name'].tolist() + \
    anno['val_images_name'].tolist()
label_annos = anno['train_label'].tolist() + anno['val_label'].tolist()

output_annotation = []
for i in range(len(image_annos)):
    image_name = image_annos[i][0][0]

    _class = label_annos[i][0]
    if _class == 1:
        _class = 2
    elif _class == 0:
        _class = 1
    else:
        _class = 0
    output_annotation.append({
        'filename': image_name,
        'class': _class
    })
with open(output_annotation_path,'w') as f:
    ujson.dump(output_annotation,f)
#%%
classes = [obj['class'] for obj in annotation]
imgs = []

for i,obj in enumerate(annotation):
    imgs.append(cv2.imread(f'dataset_bbox/Image/{obj["filename"]}'))
    if i%25==0:
        print(f'{i}/{len(annotation)}')

#%%
n = len(imgs)
n_train = int(0.8*n)
n_val= int(0.1*n)
n_test = int(0.1*n)

#%%
final_annotation = {
    'train':{
        'img': imgs[:n_train],
        'class': classes[:n_train]
    },
    'val':{
        'img': imgs[n_train:n_train+n_val],
        'class': classes[n_train:n_train+n_val]
    },
    'test':{
        'img': imgs[n_train+n_val:],
        'class': classes[n_train+n_val:]
    }
}
