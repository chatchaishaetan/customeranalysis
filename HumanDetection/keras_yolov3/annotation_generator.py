# %%
import ujson
# %%
dataset_dir = 'dataset/Image'
annotation_path = 'dataset/wider_attribute_annotation/wider_attribute_test.json'
output_path = 'model_data/human/gender_test_annotation.txt'
# %%
with open(annotation_path, 'r') as f:
    annotation_json = ujson.load(f)
# %%
category = {
    1: 1,
    -1: 2,
    0: 0
}

yolo_annotation = {}
images_info = annotation_json['images']


def create_bbox(target):
    bbox = [int(v) for v in target['bbox']]
    bbox = [bbox[0], bbox[1], bbox[0]+bbox[2], bbox[1]+bbox[3]]
    _class = category[target['attribute'][0]]
    return bbox+[_class]


for image_info in images_info:
    img_path = dataset_dir + '/' + image_info['file_name']

    yolo_annotation[img_path] = [create_bbox(
        target) for target in image_info['targets']]

# %%

def create_bbox_text(bbox):
    bbox = [str(v) for v in bbox]
    return ','.join(bbox)




lines = []
for img_path in yolo_annotation:
    bboxes = yolo_annotation[img_path]
    line = f'{img_path} {" ".join([create_bbox_text(bbox) for bbox in bboxes])}\n'
    lines.append(line)
with open(output_path, 'w') as f:
    f.writelines(lines)


#%%

