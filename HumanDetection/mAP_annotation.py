# %%
import os

# %%
yolo_annotation_path = 'model_data/human/gender_test_annotation.txt'
output_gt_dir = 'mAP/input/ground-truth'
category_path = 'model_data/human/classes.txt'
# %%
with open(category_path, 'r') as f:
    categories = f.read().split('\n')
# %%
if not os.path.exists(output_gt_dir):
    os.makedirs(output_gt_dir)

with open(yolo_annotation_path, 'r') as f:
    lines = f.read().split('\n')
    lines = lines[:-1]

for line in lines:
    img_path, *bboxes = line.split(' ')
    bboxes = [[int(v) for v in bbox.split(',')] for bbox in bboxes]
    output_annotation_path = f'{output_gt_dir}/{os.path.basename(img_path).split(".")[0]}.txt'
    with open(output_annotation_path, 'w') as f:
        f.write('\n'.join(
            [f'{categories[bbox[4]]} {" ".join([str(v) for v in bbox[:4]])}' for bbox in bboxes]))
    
# %%
