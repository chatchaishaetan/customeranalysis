# %%
import keras
import os
import numpy as np
# import keras_retinanet
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.visualization import draw_box, draw_caption
from keras_retinanet.utils.colors import label_color
import tensorflow as tf
import cv2
from pathlib import Path
import time


def get_session():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)


# keras.backend.tensorflow_backend.set_session(get_session())
# %%
# module_path = os.path.dirname(os.path.abspath(__file__))
module_path = 'HumanDetection/keras_retinanet'
model_path = os.path.join(module_path, 'snapshots',
                          'resnet50_coco_best_v2.1.0.h5')
model = models.load_model(model_path, backbone_name='resnet50')


# %%
class RetinaNet:
    def __init__(self):
        pass

    def predict(self, img, min_conf=0):
        img = preprocess_image(img)
        img, scale = resize_image(img)
        boxes, scores, labels = model.predict_on_batch(
            np.expand_dims(img, axis=0))
        boxes /= scale
        boxes = boxes[0]
        scores = scores[0]
        labels = labels[0]

        query = (labels == 0) & (scores > min_conf)
        boxes = boxes[query].astype(np.int)
        labels = labels[query]
        scores = scores[query]
        return boxes, scores, labels

    def predicts(self, imgs, min_conf=0):
        imgs = [preprocess_image(img) for img in imgs]
        tmp_imgs = imgs
        imgs = []
        scales = []
        for img in tmp_imgs:
            img, scale = resize_image(img)
            imgs.append(img)
            scales.append(scale)
        imgs = np.array(imgs)
        batch_boxes, batch_scores, batch_labels = model.predict_on_batch(imgs)
        batch_boxes /= scale
        batch_boxes = batch_boxes.astype(np.int)

        batch_boxes = [boxes for boxes in batch_boxes]
        batch_scores = [scores for scores in batch_scores]
        batch_labels = [labels for labels in batch_labels]
        for i in range(len(batch_boxes)):
            # query only box that has confidence > min_confidence and person
            query = (batch_labels[i] == 0) & (batch_scores[i] > min_conf)
            batch_boxes[i] = batch_boxes[i][query]
            batch_scores[i] = batch_scores[i][query]
            batch_labels[i] = batch_labels[i][query]

        return np.array(batch_boxes), np.array(batch_scores), np.array(batch_labels)
print('load retina')