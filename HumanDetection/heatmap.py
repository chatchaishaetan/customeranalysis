# %%
import cv2
import ujson
import numpy as np
import matplotlib.pyplot as plt
import colorsys
import tqdm
import glob
from PIL import Image
# %%


def convert_to_heatmap_img(heatmap):
    height, width = heatmap.shape
    hsv_img = np.ones((height, width, 3), dtype='uint8')*255

    max_depth = np.max(heatmap)
    hsv_img[:, :, 0] = ((heatmap/max_depth)*130).astype('uint8')

    hsv_img[heatmap == 0, 1:] = [0, 0]
    heatmap_img = cv2.cvtColor(hsv_img, cv2.COLOR_HSV2BGR)
    heatmap_img = heatmap_img[:, :, ::-1]
    return heatmap_img


hsv_img = cv2.imread('hsv.png')


def create_color_scale(width, height, thresholds, unit):
    color_bar_height = int(height*0.8)
    color_bar_width = 32
    color_bar_img = cv2.resize(hsv_img, (color_bar_width, color_bar_height))
    color_bar_img[:,:2] = (0,0,0)
    color_bar_img[:,-2:] = (0,0,0)
    

    start_color_bar_y = int(height*0.1)
    start_color_bar_x = 50
    space = np.ones((height, 400, 3), dtype='uint8')*255
    space[start_color_bar_y:start_color_bar_y+color_bar_height,
          start_color_bar_x:start_color_bar_x+color_bar_width] = color_bar_img

    ys = np.linspace(start_color_bar_y, start_color_bar_y +
                     color_bar_height, len(thresholds), dtype='int')

    for threshold, y in zip(thresholds, ys):

        cv2.line(space, (start_color_bar_x, y), (start_color_bar_x +
                                                 color_bar_width, y), (0, 0, 0), thickness=2)

        text = f'{threshold} {unit if int(threshold)==1 else unit+"s"}'
        cv2.putText(space, text, (start_color_bar_x+color_bar_width+20,
                                  y+10), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 0), 1)
    return space


def calculate_thresholds(_range, n):

    thresholds = np.linspace(_range[0], _range[1], n, dtype='int')
    thresholds = thresholds[::-1]
    return thresholds


def draw_heatmap_to_img(heatmap, img, thresholds, _range=None, unit='sec', alpha=0.7):
    heatmap = heatmap.copy()
    height, width = img.shape[:2]
    if _range is not None:
        _min, _max = _range
        heatmap[heatmap < _min] = 0
        heatmap[heatmap > _max] = _max
    heatmap_img = convert_to_heatmap_img(heatmap)

    img[heatmap > 0] = cv2.addWeighted(
        img[heatmap > 0], 1-alpha, heatmap_img[heatmap > 0], alpha, 0)

    space = create_color_scale(width, height, thresholds, unit)
    img = np.concatenate((img, space), axis=1)

    return img


# %%


def process_heatmap(footpoints, radiuses, width, height, grid_size=4):

    x_grid = np.arange(0, width, grid_size)
    y_grid = np.arange(0, height, grid_size)
    x_mesh, y_mesh = np.meshgrid(x_grid, y_grid)
    xc = x_mesh+(grid_size/2)
    yc = y_mesh+(grid_size/2)
    field = np.zeros_like(xc, dtype='uint32')

    footpoints = np.array(footpoints)
    radiuses = np.array(radiuses)*2
    for _y, row in enumerate(field):
        for _x, v in enumerate(row):
            centroid = np.array([xc[_y, _x], yc[_y, _x]])
            manhatan_distance = np.sum(np.abs(footpoints-centroid), axis=1)
            manhatan_query = manhatan_distance <= radiuses
            distances = np.sqrt(
                np.sum(np.power(footpoints[manhatan_query] - centroid, 2), axis=1))
            rs = radiuses[manhatan_query]
            query = distances <= rs

            # scores = np.abs(1-(distances[query]/rs[query]))
            dn = (distances[query]/rs[query])**2
            scores = (15/16)*((1-dn)**2)
            field[_y, _x] = np.sum(scores)
    field = np.array(Image.fromarray(field).resize(
        (width, height), Image.BICUBIC))

    return field


def process_heatmap_by_n(footpoints, ids, width, height, grid_size=32):

    x_grid = np.arange(0, width, grid_size)
    y_grid = np.arange(0, height, grid_size)
    x_mesh, y_mesh = np.meshgrid(x_grid, y_grid)
    xc = x_mesh+(grid_size/2)
    yc = y_mesh+(grid_size/2)
    field = np.empty_like(xc, dtype='object')
    for i in range(len(field)):
        for j in range(len(field[i])):
            field[i, j] = set()
    for footpoint, _id in zip(footpoints, ids):
        field[footpoint[1]//grid_size, footpoint[0]//grid_size].add(_id)

    heatmap_data = np.zeros_like(field, dtype='int32')
    for i in range(len(field)):
        for j in range(len(field[i])):
            heatmap_data[i, j] = len(field[i, j])

    heatmap_data = np.array(Image.fromarray(
        heatmap_data).resize((width, height), Image.BICUBIC))

    return heatmap_data


def process_heatmap_dets(dets_path, width, height):
    dets = ujson.load(open(dets_path, 'r'))

    length = len(dets)
    heatmap = np.zeros((height, width), dtype='float32')

    for seek in range(length):
        det = dets[seek]
        if len(det) == 0:
            continue
        det = np.array(det)
        for _object in det:
            bbox = _object[:4].astype('int')

            left, top, right, bottom = bbox
            left = max(0, left)
            top = max(0, top)
            right = min(width, right)
            bottom = min(height, bottom)

            foot_point = ((left+right)//2, bottom-1)
            foot_radius = (right-left)//4
            mask = np.zeros((height, width), dtype='uint8')
            cv2.circle(mask, foot_point, foot_radius, 1, -1)
            heatmap += mask
    return heatmap


# %%


def create_heatmap(input_dir):
    vid_paths = sorted(glob.glob(f'{input_dir}/*.mp4'))
    dets_paths = sorted(glob.glob(f'{input_dir}/*.json'))

    vid = cv2.VideoCapture(vid_paths[0])
    ret, bg_img = vid.read()
    width = int(vid.get(3))
    height = int(vid.get(4))

    heatmap = np.zeros((height, width), dtype='float32')
    for dets_path in tqdm.tqdm(dets_paths):
        heatmap += process_heatmap_dets(dets_path, width, height)
    bg_img = draw_heatmap_to_img(heatmap, bg_img)
    return bg_img

# %%


# %%


# %%
print('reload')
