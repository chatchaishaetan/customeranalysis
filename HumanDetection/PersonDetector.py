
from pathlib import Path

import numpy as np
import google.protobuf.util.json_format_proto3_pb2
import os
import ujson


module_path = os.path.dirname(os.path.abspath(__file__))
# root_model_path = module_path + '/keras_yolov3/' + 'model_data/coco'
# model = YOLO(model_path=root_model_path + '/weight.h5', anchors_path=root_model_path +
#              '/anchor.txt', classes_path=root_model_path + '/classes.txt')


class PersonDetector:
    def __init__(self, detection_path=None, use_detection_checkpoint=True, backbone='retina'):

        self.dets_json = []
        self.detection_path = Path(detection_path)
        if use_detection_checkpoint:
            if self.detection_path.exists():
                self.dets_json = self.load_detection_json()
                self.is_loaded = True
                self.is_save = False
            else:
                self.is_loaded = False
                self.is_save = True
        else:
            self.is_loaded = False
            self.is_save = False

        if 'yolo' in backbone:
            from HumanDetection.keras_yolov3.yolo import YOLO
            root_model_path = module_path + '/keras_yolov3/' + 'model_data/coco'
            self.model = YOLO(model_path=root_model_path + '/weight.h5', anchors_path=root_model_path +
                              '/anchor.txt', classes_path=root_model_path + '/classes.txt')
        elif 'retina' in backbone:
            from HumanDetection.keras_retinanet import retinanet
            self.model = retinanet.RetinaNet()

    def _fit_bbox(self, bbox, height, width):
        left, top, right, bottom = bbox
        return np.array([max(0, left), max(0, top), min(
            width, right), min(height, bottom)])

    def detect_person(self, img, seek, min_confidence=0):
        height, width = img.shape[:2]

        if self.is_loaded:
            person_bboxes = np.array(self.dets_json[seek]['bboxes'])
            person_scores = np.array(self.dets_json[seek]['scores'])
        else:

            person_bboxes, person_scores, _ = self.model.predict(
                img, min_confidence)

            person_bboxes = np.array(
                [self._fit_bbox(bbox, height, width) for bbox in person_bboxes])
        if self.is_save:
            self.dets_json.append({'bboxes': person_bboxes.tolist(),
                                   'scores': person_scores.tolist()})

        return person_bboxes, person_scores

    def detect_persons(self, imgs, start_seek, min_confidence=0):
        height, width = imgs[0].shape[:2]
        n = len(imgs)
        if self.is_loaded:
            batch_bboxes = [np.array(self.dets_json[seek]['bboxes'])
                            for seek in range(start_seek, start_seek+n)]
            batch_scores = [np.array(self.dets_json[seek]['scores'])
                            for seek in range(start_seek, start_seek+n)]
        else:
            batch_bboxes, batch_scores, _ = self.model.predicts(
                imgs, min_confidence)

            batch_bboxes =  [np.array([self._fit_bbox(bbox, height, width) for bbox in bboxes]) for bboxes in batch_bboxes]
            batch_scores = batch_scores.tolist()
        if self.is_save:
            for bboxes, scores in zip(batch_bboxes, batch_scores):
                self.dets_json.append({'bboxes': bboxes.tolist() if isinstance(bboxes,np.ndarray) else bboxes,
                                       'scores': scores.tolist() if isinstance(scores,np.ndarray) else scores})
        return batch_bboxes, batch_scores

    # det_json: [{'bboxes':..., 'scores':...}]
    def save_detection_json(self):
        with open(self.detection_path, 'w') as f:
            ujson.dump(self.dets_json, f)

    def load_detection_json(self):
        with open(self.detection_path, 'r') as f:
            dets_json = ujson.load(f)
        # dets_json = [{'bboxes': np.array(det['bboxes']), 'scores':np.array(
        #     det['scores'])} for det in dets_json]
        return dets_json
