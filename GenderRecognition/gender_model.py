# %%

import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import glob
import numpy as np
import cv2
from keras.applications import MobileNet
from keras.utils import to_categorical
import os
module_path = os.path.dirname(__file__)
# %%
input_shape = (300, 135, 3)

weight_path = module_path+'/weight/weight_augment.h5'
categories = open(module_path+'/dataset_bbox/classes.txt').read().split('\n')
# %%


def reshape(img, expected_shape):
    H, W, _ = expected_shape
    h, w, _ = img.shape
    ratio = min(H/h, W/w)

    new_img = cv2.resize(img, None, fx=ratio, fy=ratio)
    h, w, _ = new_img.shape
    space = np.ones(input_shape, dtype='uint8') * 128
    org_x = (W-w)//2
    org_y = (H-h)//2
    space[org_y:org_y+h, org_x:org_x+w] = new_img
    return space


def preprocess_x(img):
    img = reshape(img, input_shape)
    img = img / 255
    return img


def preprocess_y(label):
    return to_categorical(label, len(categories))


def post_process(y_pred):
    return np.argmax(y_pred, axis=1)


# %%
model = MobileNet(input_shape=input_shape, include_top=True,
                  weights=None, pooling='max', classes=len(categories))
model.load_weights(weight_path)
# %%


def predict_img(img):
    img = preprocess_x(img)
    pred_class = model.predict(np.array([img]))

    pred_class = post_process(pred_class)[0]
    return pred_class


def predict_img_with_confidence(img):
    img = preprocess_x(img)
    output = model.predict(np.array([img]))

    pred_class = post_process(output)[0]
    conf = output[0][pred_class]
    return pred_class, conf


def predict_imgs(imgs):
    if len(imgs) == 0:
        return []
    imgs = [preprocess_x(img) for img in imgs]
    imgs = np.array(imgs)

    pred_classes = model.predict(imgs)
    pred_classes = post_process(pred_classes)
    return pred_classes
# %%
