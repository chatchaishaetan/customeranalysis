# %%
import cv2
import pickle
import os
import glob
import numpy as np
from gender_model import model, preprocess_x, preprocess_y
import keras_metrics as km
from sklearn.metrics import confusion_matrix
# %%
# module_path = os.path.dirname(__file__)
# test_dir = module_path + '/dataset_bbox/test'
test_dir = 'dataset_bbox/test'
# %%
dataset = None
for test_path in sorted(glob.glob(test_dir+'/*.pickle')):
    if dataset is None:
        dataset = pickle.load(open(test_path, 'rb'))
    else:
        tmp_dataset = pickle.load(open(test_path, 'rb'))
        dataset['img'] += tmp_dataset['img']
        dataset['class'] += tmp_dataset['class']
        del tmp_dataset
# %%
n = 1000
imgs = dataset['img'][:n]
classes = dataset['class'][:n]
imgs = np.array([preprocess_x(img) for img in imgs])
classes = np.array([preprocess_y(_class) for _class in classes])
# %%

# %%
model.compile(optimizer='adam', loss='categorical_crossentropy',
              metrics=['acc'])
#%%

# %%

classes_pred = model.predict(imgs, batch_size=32)
#%%
metrics = model.evaluate(imgs,classes)
print(metrics)
# %%
classes_pred = np.argmax(classes_pred, axis=1)
classes_true = np.argmax(classes, axis=1)
# %%
cm = confusion_matrix(classes_true, classes_pred)
# %%
precision = np.diag(cm) / np.sum(cm, axis=0)
recall = np.diag(cm) / np.sum(cm, axis=1)
print(precision, recall)
# %%
print(precision[ ~np.isnan(precision)].mean())
print(recall[ ~np.isnan(recall)].mean())
#%%
