# %%
import pickle
import numpy as np
import glob
import tqdm
import matplotlib.pyplot as plt
from . import predict
# %%
dataset_dir = 'dataset_bbox/train'


def count_num_class(dataset_dir):
    classes = []
    for dataset_path in tqdm.tqdm(glob.glob(dataset_dir+'/*.pickle')):
        dataset = pickle.load(open(dataset_path, 'rb'))
        classes += dataset['class']
        del dataset
    num_class = {
        0: 0,
        1: 0,
        2: 0
    }
    for v in classes:
        num_class[v] += 1
    print(num_class)
    return num_class


# %%
count_num_class(dataset_dir)
# %%


def make_dataset_balance_class(dataset_dir):
    for dataset_path in tqdm.tqdm(glob.glob(dataset_dir+'/*.pickle')):
        dataset = pickle.load(open(dataset_path, 'rb'))
        dataset_by_class = {0: [], 1: [], 2: []}
        for i in range(len(dataset['img'])):
            img = dataset['img'][i]
            _class = dataset['class'][i]
            dataset_by_class[_class].append(img)
        dataset_by_class[1] = dataset_by_class[1][:len(dataset_by_class[2])]
        # %%
        new_dataset = {'img': [], 'class': []}
        for _class in dataset_by_class:
            new_dataset['img'] += dataset_by_class[_class]
            new_dataset['class'] += [
                _class for _ in range(len(dataset_by_class[_class]))]
        imgs = np.array(new_dataset['img'])
        classes = np.array(new_dataset['class'])
        shuffle = np.arange(len(imgs))
        np.random.shuffle(shuffle)
        new_dataset['img'] = imgs[shuffle].tolist()
        new_dataset['class'] = classes[shuffle].tolist()
        pickle.dump(new_dataset, open(dataset_path, 'wb'))
# %%
make_dataset_balance_class('dataset_bbox/val')
# %%
