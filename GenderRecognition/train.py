# %%
from gender_model import *
import glob
import pickle
from keras.optimizers import Adam, SGD
from keras.callbacks import ModelCheckpoint, TensorBoard
import imgaug.augmenters as iaa
import matplotlib.pyplot as plt

module_path = os.path.dirname(__file__)+'/'
module_path = ''
# %%


def create_data_generator(dataset_dir, batch_size=32, augment=False):
    if augment:
        seq = iaa.Sequential([
            iaa.Sometimes(0.5, iaa.Fliplr(1)),
            iaa.Sometimes(0.5, iaa.OneOf([
                iaa.Add(40),
                iaa.Add(-40)
            ]))
        ])
    dataset_paths = sorted(glob.glob(dataset_dir+'/*.pickle'))
    i = 0
    while True:
        dataset = pickle.load(open(dataset_paths[i], 'rb'))
        for start in range(0, len(dataset['img'])-batch_size, batch_size):
            imgs = dataset['img'][start:start +
                                  batch_size]
            if augment:
                imgs = seq.augment_images(imgs)
            try:
                imgs = [preprocess_x(img) for img in imgs]
            except:
                continue
            imgs = np.array(imgs)
            classes = dataset['class'][start:start+batch_size]
            classes = [preprocess_y(_class)
                       for _class in classes]
            classes = np.array(classes)
            yield imgs, classes
        del dataset
        i = (i+1) % len(dataset_paths)


# %%
batch_size = 32
dataset_base_path = module_path + 'dataset_bbox'
train_gen = create_data_generator(
    dataset_base_path+'/train', batch_size=batch_size, augment=True)
val_gen = create_data_generator(
    dataset_base_path+'/val', batch_size=batch_size)
# %%
model.summary()
model.compile(optimizer=Adam(),
              loss='categorical_crossentropy', metrics=['acc'])
log_dir = 'logs/t003'
checkpoint_callback = ModelCheckpoint(log_dir + '/ep{epoch:03d}-loss{loss:.3f}-val_loss{val_loss:.3f}.h5',
                                      monitor='val_loss', save_best_only=True, period=10)
tensorboard_callback = TensorBoard(log_dir=log_dir)
model.fit_generator(train_gen, steps_per_epoch=batch_size, epochs=100,
                    validation_data=val_gen, validation_steps=batch_size,
                    callbacks=[checkpoint_callback, tensorboard_callback])
# %%
