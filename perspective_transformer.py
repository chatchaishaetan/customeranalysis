# %%
import cv2
import numpy as np


# %%


class PerspectiveTransformer:
    def __init__(self,):
        self.matrix = None

    # input_rec:[top-left, top-right, bottom-right, bottom-left]
    def set_matrix(self, input_rec):
        self.output_width, self.output_height = np.max(
            input_rec, axis=0).astype('int')
        output_rec = np.float32([(0, 0), (self.output_width, 0), (
            self.output_width, self.output_height), (0, self.output_height)])
        self.matrix = cv2.getPerspectiveTransform(input_rec, output_rec)

    def tranform_image(self, img):
        return cv2.warpPerspective(img, self.matrix, (self.output_width, self.output_height))

    def tranform_points(self, points):
        transformed_points = cv2.perspectiveTransform(
            np.expand_dims(points, axis=0), self.matrix)[0]
        transformed_points_x = transformed_points[:, 0]
        transformed_points_y = transformed_points[:, 1]
        transformed_points = transformed_points[(transformed_points_x >= 0) & (transformed_points_x < self.output_width) & (
            transformed_points_y >= 0) & (transformed_points_y < self.output_height)]
        transformed_points = transformed_points.astype('int')
        return transformed_points


# %%
